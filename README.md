# CI-Deployer Docker Image

This image is intended for general purpose light scripting.
It is missing the majority of development libraries need to compile code, but contains Python 3, [deploy-eos](https://gitlab.cern.ch/ci-tools/ci-web-deployer) and [release-cli](https://docs.gitlab.com/ee/user/project/releases/release_cli.html), making it useful for deployment pipelines.

FROM debian:bookworm-slim
MAINTAINER leon.teichroeb@cern.ch

RUN apt update && apt install -y --no-install-recommends curl krb5-user xrootd-client zip unzip smbclient rsync openssh-client python3-pip python3-dev python3-venv python3-build

# Include gitlab release-cli utility, so we can use this image to create new gitlab releases.
RUN curl --location --output /usr/local/bin/release-cli "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64"
RUN chmod +x /usr/local/bin/release-cli

COPY bin/ /usr/bin
